import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable, tap} from "rxjs";
import {CookieService} from "ngx-cookie-service";
import {isDefined} from "app/utils/utils";
import {CurrentUser, LoginRequest} from "app/services/auth/auth.models";

@Injectable({
	providedIn: "root",
})
export class AuthService
{
	isDefined = isDefined;

	authUser$: Observable<CurrentUser | undefined>;
	private authUserSubject$: BehaviorSubject<CurrentUser | undefined>;

	constructor(private http: HttpClient, private cookieService: CookieService)
	{
		this.authUserSubject$ = new BehaviorSubject<CurrentUser | undefined>(AuthService.getUserFromStorage());
		this.authUser$ = this.authUserSubject$.asObservable();
	}

	private static getUserFromStorage(): CurrentUser | undefined
	{
		const localStorageEntry = localStorage.getItem("currentUser");
		// TODO: More strongly typed JSON.parse()
		return localStorageEntry ? JSON.parse(localStorageEntry) : undefined;
	}

	login$(credentials: LoginRequest): Observable<CurrentUser>
	{
		return this.http.post<CurrentUser>("https://localhost:8000/api/login", credentials)
			.pipe(
				tap((user) =>
				{
					localStorage.setItem("currentUser", JSON.stringify(user));
					this.authUserSubject$.next(user);
				}),
			)
		;
	}

	logOut(): void
	{
		this.http.delete("https://localhost:8000/api/logout");
		localStorage.removeItem("currentUser");
	}
}
