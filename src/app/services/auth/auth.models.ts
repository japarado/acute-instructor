export interface LoginRequest
{
	username: string;
	password: string;
}

export interface CurrentUser
{
	username: string;
}
