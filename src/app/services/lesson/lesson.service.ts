import {Injectable} from "@angular/core";
import {Lesson} from "app/services/lesson/lesson.models";
import {HttpClient} from "@angular/common/http";
import {Observable, of} from "rxjs";

@Injectable({
	providedIn: "root",
})
export class LessonService
{

	constructor(private http: HttpClient)
	{
	}

	getLessons$(): Observable<readonly Lesson[]>
	{
		return this.http.get<readonly Lesson[]>("https://localhost:8000/api/lessons");
	}

	getLesson$(id: number): Observable<Lesson>
	{
		console.log(`Getting lesson ${id}`);
		return this.http.get<Lesson>(`https://localhost:8000/api/lesson/${id}`);
	}
}
