import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {LoginComponent} from "./pages/login/login.component";
import {CardComponent} from "./components/card/card.component";
import {ButtonComponent} from "./components/button/button.component";
import {ReactiveFormsModule} from "@angular/forms";
import {DrawerComponent} from "./components/drawer/drawer.component";
import {DrawerItemComponent} from "./components/drawer/drawer-item/drawer-item.component";
import {NavbarComponent} from "./components/navbar/navbar.component";
import {UserInfoComponent} from "./components/user-info/user-info.component";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {HomeComponent} from "app/pages/home/home.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FooterComponent} from "./components/footer/footer.component";
import {SidebarComponent} from "app/components/sidebar/sidebar.component";
import {BaseInterceptor} from "app/base.interceptor";
import {CookieService} from "ngx-cookie-service";

@NgModule({
	declarations: [
		// Root
		AppComponent,

		// Top-level pages
		HomeComponent, LoginComponent,

		// Components
		ButtonComponent, CardComponent, DrawerComponent, DrawerItemComponent, FooterComponent, NavbarComponent, SidebarComponent, UserInfoComponent,
	],
	imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule, FontAwesomeModule, HttpClientModule],
	providers: [CookieService, {provide: HTTP_INTERCEPTORS, useClass: BaseInterceptor, multi: true}],
	bootstrap: [AppComponent],
})
export class AppModule
{
}
