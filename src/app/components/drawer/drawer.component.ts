import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
	selector: "app-drawer",
	templateUrl: "./drawer.component.html",
	styleUrls: ["./drawer.component.scss"],
})
export class DrawerComponent
{
	@Input() icon?: string;
	@Input() text?: string;
	@Output() press?: EventEmitter<void>;
}
