import {Component, Input} from "@angular/core";
import {faBell, faComment, faSearch} from "@fortawesome/free-solid-svg-icons";

@Component({
	selector: "app-user-info",
	templateUrl: "./user-info.component.html",
	styleUrls: ["./user-info.component.scss"],
})
export class UserInfoComponent
{
	faSearch = faSearch;
	faComment = faComment;
	faBell = faBell;

	// TODO: Remove this hardcoded placeholder URL value
	@Input() url: string = "https://picsum.photos/200";
	@Input() title: string = "";
}
