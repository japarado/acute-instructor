import {Component, Input} from "@angular/core";
import {TextPosition} from "app/components/typography/typography.models";

@Component({
	selector: "app-card",
	templateUrl: "./card.component.html",
	styleUrls: ["./card.component.scss"],
})
export class CardComponent
{
	TextPosition = TextPosition;

	@Input() title?: string;
	@Input() titlePosition?: TextPosition = TextPosition.LEFT;
}
