import {Component, EventEmitter, Input, Output} from "@angular/core";

export enum ButtonType
{
	PRIMARY,
	DEFAULT,
}

@Component({
	selector: "app-button",
	templateUrl: "./button.component.html",
	styleUrls: ["./button.component.scss"],
})
export class ButtonComponent
{
	ButtonType = ButtonType;

	@Input() text: string = "Click Me";
	@Input() type: ButtonType = ButtonType.DEFAULT;
	@Input() disabled?: boolean;

	@Output() buttonClick: EventEmitter<void>;

	constructor()
	{
		this.buttonClick = new EventEmitter<void>();
	}

	handleClick(): void
	{
		this.buttonClick.emit();
	}
}
