import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {LS_KEY} from "app/components/sidebar/sidebar.models";

@Injectable({
	providedIn: "root",
})
export class SidebarService
{
	showSidebar$: Observable<boolean>;
	private showSidebarSubject$: BehaviorSubject<boolean>;

	constructor()
	{
		this.showSidebarSubject$ = new BehaviorSubject<boolean>(SidebarService.getSidebarStatusFromLocalstorage());
		this.showSidebar$ = this.showSidebarSubject$.asObservable();
	}

	private static getSidebarStatusFromLocalstorage(): boolean
	{
		return localStorage.getItem(LS_KEY) === "true";
	}

	private static setSidebarStatusFromLocalstorage(status: boolean)
	{
		const value = status ? "true" : "false";
		localStorage.setItem(LS_KEY, value);
	}

	toggleSidebar(show: boolean)
	{
		SidebarService.setSidebarStatusFromLocalstorage(show);
		this.showSidebarSubject$.next(show);
	}
}
