import {Component, Input} from "@angular/core";
import {faHouseUser, faGauge} from "@fortawesome/free-solid-svg-icons";

@Component({
	selector: "app-sidebar",
	templateUrl: "./sidebar.component.html",
	styleUrls: ["./sidebar.component.scss"],
})
export class SidebarComponent
{
	faHome = faHouseUser;
	faGauge = faGauge;

	@Input() show?: boolean;

	constructor()
	{
	}
}
