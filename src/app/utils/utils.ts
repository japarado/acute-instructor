export function isNull<T>(predicate: T): boolean
{
	return predicate !== null;
}

export function isDefined<T>(predicate: T): boolean
{
	return !isNull(predicate) && predicate !== undefined;
}
