import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class BaseInterceptor implements HttpInterceptor
{

	constructor()
	{
	}

	intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>>
	{
		const modifiedRequest = request.clone({withCredentials: true});
		return next.handle(modifiedRequest);
	}
}
