import {Component, OnInit} from "@angular/core";
import {lastValueFrom, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {SidebarService} from "app/components/sidebar/sidebar.service";

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit
{
	title = "acute-instructor";

	showSidebar$: Observable<boolean>;

	constructor(private http: HttpClient, private sidebarService: SidebarService)
	{
		this.showSidebar$ = this.sidebarService.showSidebar$;
	}

	async ngOnInit()
	{
		await this.pingBackend();
	}

	handleToggleSidebar(show: boolean)
	{
		this.sidebarService.toggleSidebar(show);
	}

	private async pingBackend()
	{
		try
		{
			await lastValueFrom(this.http.get("https://localhost:8000/ping"));
			console.info("Successfully reached backend (＠＾◡＾)");
		}
		catch (e)
		{
			console.error("Cannot reach backend ｡ﾟ･ (>﹏<) ･ﾟ｡");
		}
	}
}
