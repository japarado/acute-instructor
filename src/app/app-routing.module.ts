import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./pages/login/login.component";
import {HomeComponent} from "app/pages/home/home.component";
import {AuthGuard} from "app/guards/auth.guard";

const routes: Routes = [
	{
		path: "login",
		component: LoginComponent,
		canActivate: [AuthGuard],
	},
	{
		path: "",
		component: HomeComponent,
		canActivate: [AuthGuard],
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule
{
}
