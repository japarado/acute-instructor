import {Component, OnInit} from "@angular/core";
import {ButtonType} from "app/components/button/button.component";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "app/services/auth/auth.service";
import {map, Observable, startWith} from "rxjs";
import {TextPosition} from "app/components/typography/typography.models";

@Component({
	selector: "app-login",
	templateUrl: "./login.component.html",
	styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit
{
	TextPosition = TextPosition;

	ButtonType = ButtonType;

	loginForm: FormGroup = new FormGroup({
		username: new FormControl("", Validators.required),
		password: new FormControl("", Validators.required),
	});

	isFormValid$: Observable<boolean> = new Observable<boolean>();

	constructor(private authService: AuthService)
	{
	}

	ngOnInit()
	{
		this.isFormValid$ = this.loginForm.statusChanges
			.pipe(
				map((status) => status === "VALID" && !this.loginForm.pristine),
				startWith(false),
			);
	}

	async handleSubmit()
	{
		console.log(this.loginForm.get("username"));
		console.log(this.loginForm.get("password"));
	}
}
